﻿using System;
using NLog;
using ServiceStack.Text;
using Topshelf;

namespace MyService
{
    public class WinService :ServiceControl
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        private AppHost _serviceStackHost;
        public WinService()
        {
        }

        public bool Start(HostControl hostControl)
        {
            Logger.Info("Starting Service - WinService");
            try
            {
                if (_serviceStackHost == null)
                {
                    _serviceStackHost = new AppHost();
                    _serviceStackHost.Init();
                    "ServiceStack Self Host with Razor listening at https://*/".Print();
                    _serviceStackHost.Start("https://*/");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.Read();
                return false;

            }
            Logger.Info("Started Service - WinService");
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            Logger.Info("Stopping Service - WinService");
            try
            {
                if (_serviceStackHost != null)
                {
                    _serviceStackHost.Stop();
                    _serviceStackHost = null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

            return true;
        }
    }
}