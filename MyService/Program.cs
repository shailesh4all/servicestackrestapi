﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Timers;
using NLog;
using ServiceStack.Api.Swagger;
using Topshelf;

namespace MyService
{

    public class Program
    {
        public static void Main()
        {
            try
            {
                /*
                 * To Run web api on https,
                 * 1. you will need certificate.
                 * 2. you need to bind port to certificate
                 *
                 * Step1: Open command prompt with Admin permission and run below command
                 * (locate makecert.exe on your machine. Im using VS2017 and Windows10 so my directory location is as below)
                 * cd C:\Program Files (x86)\Windows Kits\10\bin\x64
                 * this line is instruction for user to use same password on both commandline. for certificates I used password as password
                 * below command will create certificate
                 * makecert.exe -n "CN=DNB Development" -pe -r -sv TempCA.pvk TempCA.cer
                 * Open mmc.exe and load TempCA.cert under local machine\Trusted Root Certification Authorities
                 * makecert -iv TempCA.pvk -n "CN=DNB Development" -ic TempCA.cer -sr localmachine -ss my -sky exchange -pe -sv SelfHostPK.pvk SelfHost.cer 
                 * pvk2pfx -pvk "SelfHostPK.pvk" -spc "SelfHost.cer" -pfx "SelfHost.pfx" -pi sslpassword
                 * Open mmc.exe and load SelfHost.cer under local machine\Personal, right click and get cert Thumprint and put in next command
                 * netsh http add sslcert hostnameport=localhost:443 certhash=3398af61c38d8bc658dce1ac6ab77710f121b3a4 appid={16399fe7-b68d-47b4-bb90-9b13955a07c4} certstorename=MY
                 * delete SSL certificate binding with port 443
                 * netsh http delete sslcert hostnameport=localhost:443
                 */
                string certificateFile = "SelfHost.pfx";
                string str = Guid.NewGuid().ToString();
                var bindPortToCertificate = new Process();
                if (File.Exists(certificateFile))
                {
                    var bytes = File.ReadAllBytes(certificateFile);
                    var cert = new X509Certificate2(bytes, "sslpassword",
                        X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.DefaultKeySet |
                        X509KeyStorageFlags.Exportable);
                    var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                    store.Open(OpenFlags.ReadWrite);
                    var existingCert = store.Certificates.Find(X509FindType.FindByThumbprint, cert.Thumbprint, false);
                    if (existingCert.Count == 0)
                    {
                        store.Add(cert);
                    }
                    store.Close();
                    bindPortToCertificate.StartInfo.FileName =
                        Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.SystemX86), "netsh.exe");
                    bindPortToCertificate.StartInfo.Arguments =
                        $"http add sslcert hostnameport=localhost:443 certhash={cert.Thumbprint} appid={{{Guid.NewGuid()}}} certstorename=MY";
                    bindPortToCertificate.Start();
                    bindPortToCertificate.WaitForExit();

                }
                HostFactory.Run(c =>
                {
                    c.Service<WinService>();
                    c.RunAsLocalSystem();
                    c.UseNLog();
                    c.SetDescription("Rest Service");
                    c.SetDisplayName("Rest Service");
                    c.SetServiceName("Rest Service");
                    c.EnableServiceRecovery(r =>
                    {
                        r.RestartService(0);
                        r.OnCrashOnly();
                        r.SetResetPeriod(1);
                    });
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.Read();
            }

            Console.WriteLine("Complete!");
        }
    }
}
