﻿using Funq;
using ServiceStack;
using ServiceStack.Api.Swagger;
using ServiceStack.Text;

namespace MyService
{
    public class AppHost : AppSelfHostBase
    {
        public AppHost() : base("Uflow Service", typeof(AppHost).Assembly)
        {
        }

        public override ServiceStackHost Start(string urlBase)
        {
            JsConfig.DateHandler = DateHandler.ISO8601;
            var host = base.Start(urlBase);
            return host;
        }

        public override void Configure(Container container)
        {
            //            Plugins.Add(new PostmanFeature());
            //            Plugins.Add(new CorsFeature(allowedHeaders:"Content-Type, Access-Control-Allow-Headers,Authorization,Accept", allowOriginWhitelist: corslist));
            Config.WebHostUrl = "https://localhost/";
            var swaggerconfig = new SwaggerFeature();
            Plugins.Add(swaggerconfig);

        }
    }
}